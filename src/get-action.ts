import 'isomorphic-fetch';

import { ChoiceType, Question } from 'inquirer';
import {
  IData,
  parseResponse,
  parseResponseWith,
  parseXml,
  parseJson
} from 'hubris';
import { Args, CommandInstance, CallbackFunction } from 'vorpal';

import _ from 'lodash/fp';

export interface GetUrlOptions {
  strict?: boolean;
}

export interface GetUrlArgs extends Args<GetUrlOptions> {
  url: string;
}

function nonStrictParserSelection(contentType: string) {
  switch (contentType.split(';')[0]) {
    case 'application/xml':
    case 'applicaiton/vnd.uber+json':
      return parseXml;
    case 'application/json':
    case 'application/vnd.uber+json':
    default:
      return parseJson;
  }
}

const nonStrictParser = parseResponseWith(nonStrictParserSelection);

export function handle(this: CommandInstance, args: GetUrlArgs) {
  let inst = this;
  return fetch(args.url).then(resp => {
    inst.log(`${resp.status} ${resp.statusText}`);
    resp.headers.forEach((v, n) => inst.log(`${n}: ${v}`));

    return resp;
  });
}
