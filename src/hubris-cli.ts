import 'isomorphic-fetch';
import { parseResponse, parseResponseWith, parseXml, parseJson } from 'hubris';

import { handle as handleGet } from './get-action';
import { handle as handleInspect } from './inspect-action';
import Vorpal, { CommandInstance, Args } from 'vorpal';
import { Question } from 'inquirer';

import _ from 'lodash/fp';

const vorpal = new Vorpal();

vorpal.delimiter('hubris $ ').history('hubris-cli');

function nonStrictParserSelection(contentType: string) {
  switch (contentType.split(';')[0]) {
    case 'application/xml':
    case 'applicaiton/vnd.uber+json':
      return parseXml;
    case 'application/json':
    case 'application/vnd.uber+json':
    default:
      return parseJson;
  }
}

const nonStrictParser = parseResponseWith(nonStrictParserSelection);

vorpal.command('get <url>').action(handleGet);

vorpal
  .command('inspect <document> [headers...]')
  .option('-s, --strict', 'Strict Content-Type checking')
  .action(handleInspect);

vorpal.show();
