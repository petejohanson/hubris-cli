import 'isomorphic-fetch';

import { ChoiceType, Question } from 'inquirer';
import {
  follow,
  getUrl,
  IData,
  parseResponse,
  parseResponseWith,
  parseXml,
  parseJson,
  getFormFields
} from 'hubris';
import { Args, CommandInstance, CallbackFunction } from 'vorpal';

import _ from 'lodash/fp';

export interface InspectOptions {
  strict?: boolean;
}

export interface InspectArgs extends Args<InspectOptions> {
  document: string;
  headers: ReadonlyArray<string>;
}

function nonStrictParserSelection(contentType: string) {
  switch (contentType.split(';')[0]) {
    case 'application/xml':
    case 'applicaiton/vnd.uber+json':
      return parseXml;
    case 'application/json':
    case 'application/vnd.uber+json':
    default:
      return parseJson;
  }
}

const nonStrictParser = parseResponseWith(nonStrictParserSelection);

function parseStdin(input: string) {
  const lines = input.split('\n');

  const status = lines.shift();
  const headers = new Headers();

  while (lines.length > 0) {
    const h = lines.shift();
    if (!h || h === '') {
      break;
    }

    const [name, value] = h.split(':', 2);
    headers.append(name, value);
  }

  // TODO: STATUS!
  return new Response(lines.join('\n'), { headers });
}

export function handle(
  this: CommandInstance,
  args: InspectArgs,
  callback: CallbackFunction<void>
) {
  let resp;

  if (args.document === '-') {
    if (!args.previous) {
      throw new Error('No previous Response to inspect');
    }

    resp = args.previous as Response;

    if (!resp) {
      throw new Error('Unable to inspect non-Response object');
    }
  } else {
    const headers = new Headers(args.headers.map(s => s.split(':', 2)));

    resp = new Response(args.document, { headers });
  }

  return processResponse(this, args.options)(resp);
}

// Options from a doc:
// * View errors
// * View data
// * Quit
//

// When viewing a data parent, display all items, and allow selecting one.

// Once you've selected a specific data item:
// 1. Display full details.
// 2. Actions you can do:
//    a. View children.
//    b. Follow (Use sub-fields to drive input!). Results fed back in to processResponse.

// Piping?!

enum DocActions {
  ViewData = 'view_data',
  ViewErrors = 'view_errors',
  Quit = 'quit'
}

function viewData(instance: CommandInstance, data: IData[]) {
  const question: Question = {
    type: 'list',
    choices: data.map((d, i) => {
      const { name, rel, id, model, url, data } = d;

      const display: any = {
        name,
        rel,
        id,
        model,
        url: url ? url.toString() : undefined
      };

      if (data) {
        display['data'] = `${data.length} items`;
      }

      return {
        name: JSON.stringify(display),
        value: i.toString(),
        extra: d
      };
    }),
    name: 'data-selection',
    message: 'Select a data element'
  };

  return instance
    .prompt(question)
    .then(answers => data[parseInt(answers['data-selection'] as string, 10)])
    .then(inspectDataElement(instance))
    .then(_.tap(console.log));
}

type FormInput = {
  [key: string]: string | string[] | { [key: string]: string };
};
function getFormFieldValue(
  field: IData,
  instance: CommandInstance,
  input: FormInput
) {
  return instance
    .prompt({
      type: 'input',
      name: 'value',
      message: `Please enter a value for ${field.label || field.name}: `,
      default: input[field.name || '']
    })
    .then(answers => answers['value']);
}

function getFormFieldValues(
  data: IData,
  fields: ReadonlyArray<IData>,
  instance: CommandInstance,
  input: FormInput
): PromiseLike<FormInput> {
  const choices: ChoiceType[] = fields.map(f => ({
    name: `${f.label || f.name || ''}: ${input[f.name || ''] || ''}`,
    value: f.name
  }));
  choices.push({ name: 'Done', value: undefined });

  return instance
    .prompt({
      type: 'list',
      name: 'field',
      message: 'Select a field to enter/edit the value',
      choices
    })
    .then(answers => {
      const name = answers['field'] as string;

      if (!name) {
        return input;
      } else {
        const field = fields.find(f => f.name === name);
        if (!field) {
          return input;
        }

        return getFormFieldValue(field, instance, input).then(value => {
          input[field.name || ''] = value;
          return getFormFieldValues(data, fields, instance, input);
        });
      }
    });
}

function followForm(data: IData, fields: IData[], instance: CommandInstance) {
  return getFormFieldValues(data, fields, instance, {}).then(
    input => follow(data, input) as PromiseLike<Response>
  );
}

enum DataElementAction {
  ViewDetails = 'view_details',
  ViewChildren = 'view_children',
  Follow = 'follow',
  Quit = 'quit'
}

const DATA_FIELD_LABELS: { readonly [key in keyof IData]: string } = {
  id: 'ID',
  name: 'Name',
  label: 'Label',
  value: 'Value',
  url: 'URL',
  rel: 'Relations',
  action: 'Action (METHOD)',
  model: 'Form Model',
  accepting: 'Accepted Media Types',
  sending: 'Sent Media Types',
  data: 'Child Elements',
  transclude: 'Transclude'
};

function viewDataDetails(instance: CommandInstance, data: IData) {
  const displayFields: Array<keyof IData> = [
    'id',
    'name',
    'label',
    'rel',
    'value',
    'url',
    'action',
    'model',
    'accepting',
    'sending',
    'data',
    'transclude'
  ];

  // TODO: Instead, serialize back to XML/JSON for display?
  instance.log('Data Element Details -');
  instance.log();
  displayFields.forEach(f => {
    const v = data[f];
    if (v) {
      instance.log(`${DATA_FIELD_LABELS[f]}: `, v.toString());
    }
  });

  return inspectDataElement(instance)(data);
}

function inspectDataElement(
  instance: CommandInstance
): (data: IData) => PromiseLike<string> {
  return (data: IData) => {
    const choices: ChoiceType[] = [
      { name: 'View Details', key: 'v', value: DataElementAction.ViewDetails }
    ];

    if (data.data) {
      choices.push({
        name: 'View Children',
        key: 'c',
        value: DataElementAction.ViewChildren
      });
    }

    if (data.url) {
      choices.push({
        name: 'Follow Link',
        key: 'f',
        value: DataElementAction.Follow
      });
    }

    choices.push({ name: 'Quit', key: 'q', value: DataElementAction.Quit });

    const question: Question = {
      type: 'expand',
      name: 'data_action',
      message: 'What would you like to do w/ this data elmeent?',
      choices
    };

    return instance.prompt(question).then(answers => {
      const action = answers['data_action'] as DataElementAction;
      switch (action) {
        case DataElementAction.ViewDetails:
          return viewDataDetails(instance, data);
        case DataElementAction.ViewChildren:
          return viewData(instance, data.data || []);
        case DataElementAction.Follow:
          const fields = getFormFields(data);
          const resp = fields.length
            ? followForm(data, fields, instance)
            : follow(data, {});

          return resp.then(processResponse(instance, {}));
        default:
          return Promise.resolve('');
      }
    });
  };
}
export function processResponse(
  instance: CommandInstance,
  options: InspectOptions
) {
  return (response: Response) => {
    if (!response.ok) {
      throw new Error(`Error: ${response.status} ${response.statusText}`);
    }

    if (response.status === 204) {
      return Promise.resolve('Empty Response');
    }

    return (options.strict ? parseResponse : nonStrictParser)(
      response
    ).then(function(doc) {
      const dataCount = (doc.data && doc.data.length) || 0;
      const errorCount =
        (doc.error && doc.error.data && doc.error.data.length) || 0;

      instance.log();
      instance.log(`Uber Document (v${doc.version}) Response.`);
      instance.log(`Errors: ${errorCount}`);
      instance.log(`Data:   ${dataCount}`);
      instance.log();

      const docChoices: Array<ChoiceType> = [];

      if (doc.error && doc.error.data && doc.error.data.length) {
        docChoices.push({
          name: 'View Errors',
          value: DocActions.ViewErrors,
          extra: doc.error.data
        });
      }

      if (doc.data && doc.data.length) {
        docChoices.push({
          name: 'View Data',
          value: DocActions.ViewData,
          extra: doc.data,
          key: 'd'
        });
      }

      docChoices.push({ name: 'Quit', value: DocActions.Quit, key: 'q' });

      return instance
        .prompt({
          type: 'expand',
          name: 'action',
          message: 'Doc options?',
          choices: docChoices,
          default: 0
        })
        .then(answers => {
          const action = answers['action'] as DocActions;

          switch (action) {
            case DocActions.ViewData:
              return viewData(instance, doc.data!);
            case DocActions.ViewErrors:
              return viewData(instance, doc!.error!.data!);
            case DocActions.Quit:
            default:
              return;
          }
        });
    });
  };
}
